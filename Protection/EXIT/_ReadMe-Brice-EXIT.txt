E.X.I.T
--------------------------------------------------------------------------------
RUN"UBI
RUN"UBI.BIN does not work
--------------------------------------------------------------------------------
Track &0F has a bunch of " STARTER " at the end of each gap2s but it does not seem to be used by the protection.
--------------------------------------------------------------------------------

Le header du loader:

│00001200-> 00 55 42 49 20 20 20 20 20 42 41 53 00 00 00 00   .UBI     BAS....  
 00001210-> 00 00 02 00 00 00 40 00 62 03 26 43 00 00 00 00   ......@.b.&C....

-> Load address 4000
-> Exec address 4326

--------------------------------------------------------------------------------
Mail à Pierre Guerrier du 1er Janvier 1999

Salut,

Je viens de passer quelques heures a deplomber EXIT.
C'est un jeu que j'avais edite chez UBI, mais je n'avais pas ecrit la protection.
Il s'avere que la protection est basee sur le fait que la piste 0 est formatee ave un GAP#3
 plus grand que normal (72 au lieu de 54).
Le truc que je ne savais pas, c'est que quand on fait un ReadTrack avec (00 00 FF FF FF FF FF)
 comme parametres, le FDC lit le premier secteur suivit de tout ce qu'il trouve sur la piste
 (y compris le GAP#3, les CRC, etc.).
Ca veut dire que, pour emuler ca, il faudrait mesurer la taille du GAP#3 et l'encoder correctement
 dans le .edsk. Je doute que quiconque se soit emmerde a faire ca jusqu'ici.
J'ai aussi vu (sur le meme disque), des tracks qui contiennent le mot "STARTER" au milieu du GAP#2
 (c'est sense etre 22 octets 4E)!!!



--------------------------------------------------------------------------------
Protection soft:
Commence en 4326 et finit par un jump en E000

4326 DI

4327 LD   BC,7F0F
432A LD   A,54
432C OUT  C,(C) [Ga]
432E OUT  A,(C) [Ga]
4330 DEC  C
4331 JP   P,432C

4334 LD   HL,4341
4337 LD   DE,F6F6
433A LD   BC,0040
433D PUSH DE
433E LDIR
4340 RET

F6F6 DI
F6F7 LD   SP,F6C0
F6FA LD   HL,0000
F6FD LD   DE,436B
F700 PUSH DE
F701 LD   A,04
F703 LD   R,A
F705 LD   A,R
F707 XOR  (HL)
F708 LD   (HL),A
F709 INC  HL
F70A JP   NC,F705
...
F70A JP   NC,F705
F705 LD   A,R
F707 XOR  (HL)
F708 LD   (HL),A
F709 INC  HL
F70A JP   NC,F705

F705 RET

4311 LD   BC,7FC0
4314 OUT  C,(C) [Ga]
4316 LD   SP,BFFE
4319 LD   HL,429D
431C LD   DE,D000
431F LD   BC,0074
4322 PUSH DE
4323 LDIR
4325 RET

D000 LD   IX,4000
D004 LD   HL,3E80
D007 LD   DE,C000
D00A LD   C,50
D00C CALL D049
	D049 LD   A,(IX+00)
	D04C RET
D00F CALL D04D
	D04D XOR  iX
	D04F XOR  Ix
	D051 XOR  L
	D052 XOR  H
	D053 INC  IX
	D055 RET
D012 LD   (0038),HL
D015 LD   B,A
D016 RLA
D017 JR   NC,D036

D02E LD   A,H
D02F OR   L
D030 JP   NZ,D00C
D033 JP   E000 		 -> Fin de la protection soft

D036 CALL D049
	D049 LD   A,(IX+00)
	D04C RET
D039 CALL D04D
	D04D XOR  iX
	D04F XOR  Ix
	D051 XOR  L
	D052 XOR  H
	D053 INC  IX
	D055 RET
D03C LD   (DE),A
D03D INC  DE
D03E DEC  HL
D03F PUSH AF
D040 CALL D056
D043 POP  AF
D044 DJNZ D03C
D046 JP   D02E
	D056 LD   A,E
	D057 CP   C
	D058 RET  NZ
	D059 PUSH HL
	D05A LD   HL,07B0
	D05D ADD  HL,DE
	D05E EX   DE,HL
	D05F POP  HL
	D060 LD   A,D
	D061 CP   7B
	D063 RET  NC
	D064 PUSH HL
	D065 LD   HL,0050
	D068 ADD  HL,DE
	D069 EX   DE,HL
	D06A POP  HL
	D06B LD   A,C0
	D06D OR   D
	D06E LD   D,A
	D06F LD   A,50
	D071 ADD  C
	D072 LD   C,A
	D073 RET

	--------------------------------------------------------------------------------

Protection hard:

E000 DI
E001 CALL E023			; read 400 bytes off of track 0 and store them in 4000
E004 LD   HL,4202		; First gap byte (after 2-byte CRC)
E007 LD   BC,00FF		; count until a 00 or a FF
E00A LD   E,FF
E00C LD   A,(HL)
E00D INC  HL
E00E INC  E
E00F CP   C
E010 JR   Z,E015
E012 CP   B
E013 JR   NZ,E00C
E015 LD   A,E
E016 AND  FE
E018 CP   72			; did we count &72 gap bytes (&4E)
E01A JP   Z,E0CA		; Good!!!
E01D LD   BC,7F89
E020 OUT  C,(C) [Ga]
E022 RST  0[RESET]		; BOOM!!!!

Here's the intersector data at 4200:
│00000000-> 7C 09 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E   |.NNNNNNNNNNNNNN  
 00000010-> 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E   NNNNNNNNNNNNNNNN
 00000020-> 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E   NNNNNNNNNNNNNNNN
 00000030-> 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E   NNNNNNNNNNNNNNNN
 00000040-> 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E   NNNNNNNNNNNNNNNN
 00000050-> 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E   NNNNNNNNNNNNNNNN
 00000060-> 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E   NNNNNNNNNNNNNNNN
 00000070-> 4E 4E 4E 4E 00 00 00 00 00 00 00 00 00 00 00 00   NNNN............
 00000080-> 1A 1A 1A FE 00 00 C2 02 45 AC 4E 4E 4E 4E 4E 4E   ........E.NNNNNN
 00000090-> 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E 4E   NNNNNNNNNNNNNNNN
 000000A0-> 00 00 00 00 00 00 00 00 00 00 00 00 1A 1A 1A FB   ................
 000000B0-> 00 38 06 1F 01 08 01 0B 15 06 14 15 00 00 00 00   .8..............
 000000C0-> 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
 000000D0-> 00 58 06 1F 01 07 01 0B 15 06 14 15 00 00 00 00   .X..............
 000000E0-> 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00   ................
 000000F0-> 00 39 06 1F 01 06 01 0B 15 06 14 15 00 00 00 00   .9..............

The key is that the Gap#3 on track 0 is 0x72 instead of 0x42

; read 400 bytes off of track 0 and store them in 4000
E023 LD   BC,FA7E
E026 PUSH BC
E027 LD   A,01
E029 OUT  A,(C) [FDC motor]
E02B LD   HL,E0B4	; 02 4A 00 - ReadID
E02E CALL E06C		; -> 00 00 00 00 00 C6 02
E031 LD   A,(BE4C)
E034 BIT  3,A		; Drive not ready?
E036 JR   NZ,E02B
E038 LD   HL,E0B9	; 02 07 00 - Recalibrate
E03B CALL E06C 		; execute FDC command
E03E LD   HL,E0B7	; 01 08 - SenseInt
E041 CALL E06C 		; execute FDC command
E044 LD   A,(BE4C)	; ST3
E047 BIT  5,A		; Seek End ?
E049 JR   Z,E03E
E04B LD   HL,E0BC	; 03 0F 00 00 - Find Track
E04E CALL E06C 		; execute FDC command
E051 LD   HL,E0B7	; 01 08 - SenseInt
E054 CALL E06C 		; execute FDC command
E057 LD   A,(BE4C)	; ST3
E05A BIT  5,A		; Seek End?
E05C JR   Z,E051
E05E LD   HL,E0C0	; 09 42 00 00 00 FF FF FF FF FF - Read track
E061 LD   DE,4000
E064 CALL E06F 		; execute FDC command -> 4000
E067 POP  BC
E068 XOR  A
E069 OUT  A,(C) [FDC motor]
E06B RET

; execute FDC command (data in BE4C, up to 0400 bytes)
E06C LD   DE,BE4C
; execute FDC command at HL, data to DE, up to 0400 bytes
E06F LD   B,(HL)
E070 INC  HL
E071 PUSH BC
E072 LD   A,(HL)
E073 INC  HL
E074 CALL E096 ; write byte to FDC
E077 POP  BC
E078 DJNZ E071
E07A LD   BC,FB7E
E07D LD   HL,0400
E080 IN   A,(C) [FDC Status]
E082 BIT  4,A
E084 RET  Z
E085 CP   C0
E087 JR   C,E080
E089 INC  C
E08A IN   A,(C) [FDC Data]
E08C LD   (DE),A
E08D INC  DE
E08E DEC  C
E08F DEC  L
E090 JR   NZ,E080
E092 DEC  H
E093 JR   NZ,E080
E095 RET

; write byte to FDC
E096 PUSH AF
E097 PUSH AF
E098 LD   BC,FB7E
E09B IN   A,(C) [FDC Status]
E09D ADD  A
E09E JR   NC,E09B
E0A0 ADD  A
E0A1 JR   NC,E0A6
E0A6 POP  AF
E0A7 INC  C
E0A8 OUT  A,(C) [FDC Data]
E0AA DEC  C
E0AB LD   A,0A
E0AD DEC  A
E0AE JR   NZ,E0AD
E0B0 POP  AF
E0B1 RET

